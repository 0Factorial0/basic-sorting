def clear():
    import os
    #clear screen
    test_os = os.name
    if test_os == "nt":
        os.system('cls')
    elif test_os == "posix":
        os.system('clear')
    else:
        pass

def write():

    import time
    import os

    #clear screen
    clear()

    #default variables
    isUserWriting = True
    row_count = 1
    line_list = []

    #get the current location of folder
    notepad_file_name = "notepad.py"
    file_path = os.path.dirname(os.path.abspath(notepad_file_name))
    
    #menu
    print("--------------------------")
    print("PYX Notepad")
    print("--------------------------")
    print("Editing File")
    print("--------------------------")

    try:
        #get user input
        while isUserWriting == True:
            userinput0 = str(input("Write Line [{0}]: ".format(row_count)))
            #if //end is written it's finish line
            if userinput0 == "//end":
                isUserWriting = False
            else:
                row_count = row_count + 1
                line_list.append(userinput0)

        #write the lines
        file = open(file_path+"/array.txt", "w")
        for line in line_list:
            file.write("%s\n" % line)
        file.close()

        #pass to sorted file
        os.system('type array.txt> array_sorted.txt')
    except:
        print("--------------------------")
        print("Unicode Error. Only Use English Characters Please.")
        print("--------------------------")
        time.sleep(4)

    #return notepad
    time.sleep(2)
    main()

def sort():

    import os

    #clear screen
    clear()

    #sort the file and pass
    os.system('type array.txt | sort> array_sorted.txt')

    #route back
    main()

def read():

    import os

    #clear screen
    clear()

    #menu
    print("--------------------------")
    print("PYX Notepad")
    print("--------------------------")
    print("Reading File")
    print("--------------------------")

    #get the file
    os.system('type array_sorted.txt')
    print("--------------------------")
    print("Press Enter To Return...")
    print("--------------------------")
    input("")

    #route back
    main()

def main():
    
    import time

    #clear screen
    clear()

    #main menu
    print("--------------------------")
    print("Press 1 For Sort The File")
    print("Press 2 For Write To File")
    print("Press 3 For Read The File")
    print("--------------------------")

    #get input
    try:
        userinput0 = int(input("Input: "))
        if userinput0 < 1 or userinput0 > 3:
            print("--------------------------")
            print("Input Error.")
            print("--------------------------")
            time.sleep(2)
            main()
    except:
        print("--------------------------")
        print("Input Error.")
        print("--------------------------")
        time.sleep(2)
        main()

    if userinput0 == 1:
        sort()
    elif userinput0 == 2:
        write()
    elif userinput0 == 3:
        read()

main()