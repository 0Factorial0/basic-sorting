# Sorting In MS-DOS
Sorting A to Z:
`type random_words.txt | sort> words_sorted.txt`

Sorting Z to A:
`type random_words.txt | sort /r> words_sorted_desc.txt`

Numbers or Booleans are not included.